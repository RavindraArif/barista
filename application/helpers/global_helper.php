<?php 


function cek_login()
{
   $ci = & get_instance();

   if(!$ci->session->userdata('logged_as')){
      redirect(base_url().'Login');
   }else{
      $ci->data["LOGGED_AS"] = $ci->session->userdata('logged_as');

      if($ci->data["LOGGED_AS"] == "ADMINROOT"){
         $ci->data["UPDATED_BY"] =  $ci->session->userdata('username')." - ".$ci->session->userdata('nama');
      }

      if($ci->data["LOGGED_AS"] == "DIREKSI"){
         $ci->data["UPDATED_BY"] =  $ci->session->userdata('username')." - ".$ci->session->userdata('nama_direksi');
      }

      if($ci->data["LOGGED_AS"] == "SEKDIR"){
         $ci->data["UPDATED_BY"] =  $ci->session->userdata('username')." - ".$ci->session->userdata('nama_sekdir');
      }

      if($ci->data["LOGGED_AS"] == "PROTOKOL"){
         $ci->data["UPDATED_BY"] =  $ci->session->userdata('username')." - ".$ci->session->userdata('nama_protokol');
      }
   }
}


?>