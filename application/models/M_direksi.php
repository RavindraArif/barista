<?php

class M_direksi extends CI_Model{
    public function get()
    {
        //YANG TAK DIDELETE
        $this->db->where("t_direksi.deleted","0");

        return $this->db->select('t_direksi.*,t_direksi_sekdir.id_sekdir,t_sekdir.nama_sekdir')
                ->from('t_direksi')
                ->join('t_direksi_sekdir','t_direksi.id_direksi = t_direksi_sekdir.id_direksi','left')
                ->join('t_sekdir','t_sekdir.id_sekdir = t_direksi_sekdir.id_sekdir','left')
                ->get();
    }

  function set($dataPost){

        if($dataPost["password"] == ""){
        unset($dataPost["password"]);
        }

        if ($dataPost['proc'] == 'insert') {
        
        $id_sekdir = $dataPost['id_sekdir'];
        unset($dataPost['proc']);
        unset($dataPost['id']);
        unset($dataPost['id_sekdir']);
        
        $this->db->trans_start();

        $this->db->insert("t_direksi",$dataPost);

        $last_id = $this->db->insert_id();

        $direksi_sekre_data = array('id_direksi' => $last_id, 'id_sekdir' => $id_sekdir);
        $this->db->insert("t_direksi_sekdir",$direksi_sekre_data);
        
        $this->db->trans_complete();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($last_id);
        }

        if ($this->db->trans_status()) {
            return true;
        }else{
            return false;
        }

        }elseif ($dataPost['proc'] == 'update') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        $id_sekdir = $dataPost['id_sekdir'];
        unset($dataPost['id']);
        unset($dataPost['id_sekdir']);

        $this->db->trans_start();
        $this->db->update("t_direksi" ,$dataPost, array('id_direksi' => $dataid));
        
        $this->db->where('id_direksi', $dataid);
        $query = $this->db->get('t_direksi_sekdir');
        if($query->num_rows() >= 1)
        {   
            $direksi_sekre_data = array('id_sekdir' => $id_sekdir);
            $this->db->update("t_direksi_sekdir" ,$direksi_sekre_data, array('id_direksi' => $dataid));
        }else{
            $direksi_sekre_data = array('id_direksi' => $dataid, 'id_sekdir' => $id_sekdir);
            $this->db->insert("t_direksi_sekdir",$direksi_sekre_data);
        }
        $this->db->trans_complete();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($dataid);
        }

        if ($this->db->trans_status()) {
            return true;
        }else{
            return false;
        }
        
        }elseif ($dataPost['proc'] == 'delete') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);

        $result = $this->db->update("t_direksi", array('deleted' => 1), array('id_direksi' => $dataid));
        // $result = $this->db->delete("t_direksi", array('id_direksi' => $dataid));
        if ($result) {
            return true;
        }else{
            return false;
        }
        }else{
        return false;
        }


    }



  private function uploadkan($iddata)
  { 

      $filename                       = "DIR_".uniqid();
      $config['upload_path']          = './UPLOADS/';
      $config['allowed_types']        = 'jpeg|jpg|gif|png';
      $config['max_size']             = 1000;
      $config['file_name']            = $filename;
    

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('foto'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $update_array = array(
            'foto' => $this->upload->data()['file_name'], 
          );

          $this->db->where("id_direksi", $iddata);
          $this->db->update("t_direksi", $update_array);
      }

  }



}