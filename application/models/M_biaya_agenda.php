<?php

class M_biaya_agenda extends CI_Model{
    public function get($id_agenda)
    {

        $this->db->join("t_harga","t_biaya_agenda.id_harga = t_harga.id_harga","LEFT");

        $this->db->where("id_agenda", $id_agenda);

        return $this->db->get('t_biaya_agenda');
    }

  function set($dataPost){

        // if($dataPost["password"] == ""){
        // unset($dataPost["password"]);
        // }

        if ($dataPost['proc'] == 'insert') {

        unset($dataPost['proc']);
        unset($dataPost['id']);
        
        $result = $this->db->insert("t_biaya_agenda",$dataPost);

        $last_id = $this->db->insert_id();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($last_id);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }

        }elseif ($dataPost['proc'] == 'update') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);
        
        

        $result = $this->db->update("t_biaya_agenda" ,$dataPost, array('id_biaya_agenda' => $dataid));

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($dataid);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }
        
        }elseif ($dataPost['proc'] == 'delete') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);

        $result = $this->db->delete("t_biaya_agenda", array('id_biaya_agenda' => $dataid));

        if ($result) {
            return true;
        }else{
            return false;
        }
        }else{
        return false;
        }


    }



  private function uploadkan($iddata)
  { 

      $filename                       = "BIAYA_".uniqid();
      $config['upload_path']          = './UPLOADS/';
      $config['allowed_types']        = 'jpeg|jpg|gif|png';
      $config['max_size']             = 1000;
      $config['file_name']            = $filename;
    

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('foto'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $update_array = array(
            'foto' => $this->upload->data()['file_name'], 
          );

          $this->db->where("id_biaya_agenda", $iddata);
          $this->db->update("t_biaya_agenda", $update_array);
      }

  }



}