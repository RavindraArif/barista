<?php

class M_agenda extends CI_Model{

    private function userblok()
    {
        //YANG USER ITU PUNYA
        $LOGGED_AS = $this->data["LOGGED_AS"];

        if($LOGGED_AS == "DIREKSI"){
            $USERDATA = $this->session->userdata();
            $this->db->where("id_direksi", $USERDATA['id_direksi']);
            $this->db->select("t_direksi_agenda.id_agenda");
            $data = $this->db->get("t_direksi_agenda")->result_array();
            $data_id_agenda = array();
            if(count($data) > 0){
                $data = $data;
                foreach ($data as $key => $value) {
                    array_push($data_id_agenda, $value["id_agenda"]);
                }
            }else{
                $data = array();
                
            }

            $this->db->where_in("id_agenda", $data_id_agenda);

       
           
        }

        if($LOGGED_AS == "PROTOKOL"){
            $USERDATA = $this->session->userdata();
            $this->db->where("id_protokol", $USERDATA['id_protokol']);
            $this->db->select("t_protokol_agenda.id_agenda");
            $data = $this->db->get("t_protokol_agenda")->result_array();
            $data_id_agenda = array();
            if(count($data) > 0){
                $data = $data;
                foreach ($data as $key => $value) {
                    array_push($data_id_agenda, $value["id_agenda"]);
                }
            }else{
                $data = array();
            }
            $this->db->where_in("id_agenda", $data_id_agenda);
            
        }

    }

    private function userblok_konfirmasi()
    {
        //YANG USER ITU PUNYA
        $LOGGED_AS = $this->data["LOGGED_AS"];

        if($LOGGED_AS == "DIREKSI" && $this->uri->segment(2) == "KonfirmasiAgenda"){
            $USERDATA = $this->session->userdata();
            $this->db->where("t_direksi_agenda.id_direksi", $USERDATA["id_direksi"]);
        }

    }

    public function get()
    {
        
        $this->userblok();

        //YANG TAK DIDELETE
        $this->db->where("t_agenda.deleted","0");

        return $this->db->get('t_agenda');
    }

    public function getBiayaAgenda()
    {

        $this->userblok();

        $this->db->select("
            t_agenda.*,
            t_agenda.id_agenda as idx,
            (
                select sum(t_harga.harga * t_biaya_agenda.qty) from t_biaya_agenda LEFT JOIN t_harga ON t_biaya_agenda.id_harga = t_harga.id_harga where t_biaya_agenda.id_agenda = idx
            ) as total_biaya
        ");

        $this->db->where("deleted","0");

        return $this->db->get('t_agenda');
    }

    public function getByDate($tanggal_agenda)
    {

        $this->userblok();

        $this->db->select("
        t_agenda.*,
        t_agenda.id_agenda as idx,
        (
            select count(*) from t_direksi_agenda where id_agenda = idx
        ) as jumlah_direksi,
        (
            select count(*) from t_direksi_agenda where id_agenda = idx and konfirmasi = 1
        ) as jumlah_konfirmasi,
        (
            select count(*) from t_protokol_agenda where id_agenda = idx
        ) as jumlah_protokol
        
        ");

        $this->db->where("waktu_agenda >=", $tanggal_agenda." 00:00:00");
        $this->db->where("waktu_agenda <=", $tanggal_agenda." 23:59:59");

        //YANG TAK DIDELETE
        $this->db->where("deleted","0");

        return $this->db->get('t_agenda');
    }

    public function getByID($id_agenda)
    {
        $this->db->select("
        t_agenda.*,
        t_agenda.id_agenda as idx,
        (
            select count(*) from t_direksi_agenda where id_agenda = idx
        ) as jumlah_direksi,
        (
            select count(*) from t_direksi_agenda where id_agenda = idx and konfirmasi = 1
        ) as jumlah_konfirmasi,
        (
            select count(*) from t_protokol_agenda where id_agenda = idx
        ) as jumlah_protokol,
        (
            select sum(t_harga.harga * t_biaya_agenda.qty) from t_biaya_agenda LEFT JOIN t_harga ON t_biaya_agenda.id_harga = t_harga.id_harga where t_biaya_agenda.id_agenda = idx
        ) as total_biaya

        
        ");

        $this->db->where("id_agenda",$id_agenda);

        //YANG TAK DIDELETE
        $this->db->where("deleted","0");

        return $this->db->get('t_agenda');
    }

    public function getDireksi($id_agenda)
    {
        // $this->userblok_konfirmasi();

        $this->db->join("t_direksi","t_direksi_agenda.id_direksi = t_direksi.id_direksi", "LEFT");
        $this->db->where("t_direksi_agenda.id_agenda", $id_agenda);
        return $this->db->get('t_direksi_agenda');
    }

    public function getSekretaris($id_agenda)
    {
        
        // $this->userblok_konfirmasi();
        $LOGGED_AS = $this->data["LOGGED_AS"];
        if($LOGGED_AS == "SEKDIR"){
            $this->db->where("t_sekdir_agenda.id_sekdir", $this->session->userdata('id_sekdir'));
        }
        $this->db->join("t_sekdir","t_sekdir_agenda.id_sekdir = t_sekdir.id_sekdir", "LEFT");
        $this->db->join("t_agenda","t_sekdir_agenda.id_agenda = t_agenda.id_agenda", "LEFT");
        if($id_agenda != '0'){
            $this->db->where("t_sekdir_agenda.id_agenda", $id_agenda);
        }   
        return $this->db->get('t_sekdir_agenda');
    }

    public function getProtokol($id_agenda)
    {
        $LOGGED_AS = $this->data["LOGGED_AS"];
        if($LOGGED_AS == "PROTOKOL"){
            $this->db->where("t_protokol_agenda.id_protokol", $this->session->userdata('id_protokol'));
        }

        $this->db->join("t_protokol","t_protokol_agenda.id_protokol = t_protokol.id_protokol", "LEFT");
        $this->db->join("t_agenda","t_protokol_agenda.id_agenda = t_agenda.id_agenda", "LEFT");
        if($id_agenda != '0'){
            $this->db->where("t_protokol_agenda.id_agenda", $id_agenda);
        }
        return $this->db->get('t_protokol_agenda');
    }

  function set($dataPost){


        if ($dataPost['proc'] == 'insert') {

        unset($dataPost['proc']);
        unset($dataPost['id']);
        
        $result = $this->db->insert("t_agenda",$dataPost);

        $last_id = $this->db->insert_id();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($last_id);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }

        }elseif ($dataPost['proc'] == 'update') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);
        
        

        $result = $this->db->update("t_agenda" ,$dataPost, array('id_agenda' => $dataid));

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($dataid);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }
        
        }elseif ($dataPost['proc'] == 'delete') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);

        $result = $this->db->update("t_agenda", array('deleted' => 1), array('id_agenda' => $dataid));

        if ($result) {
            return true;
        }else{
            return false;
        }
        }else{
        return false;
        }


    }

    public function setDireksi($dataPost)
    {
        if ($dataPost['proc'] == 'insert') {

            unset($dataPost['proc']);
            unset($dataPost['id']);
            
            echo json_encode($dataPost);
            $this->db->trans_start();

            $result = $this->db->insert("t_direksi_agenda",$dataPost);
            $last_id = $this->db->insert_id();
            
            $this->db->select('id_sekdir');
            $this->db->where('id_direksi',$dataPost['id_direksi']);
            $id_sekdir = $this->db->get('t_direksi_sekdir')->row();

            $dataSekdir = array('id_agenda' => $dataPost['id_agenda'], 'id_sekdir' => $id_sekdir->id_sekdir);
            $this->db->insert("t_sekdir_agenda",$dataSekdir);

            $this->db->trans_complete();

            if ($this->db->trans_status()) {
                return true;
            }else{
                return false;
            }
    
            }elseif ($dataPost['proc'] == 'update') {
            unset($dataPost['proc']);
            $dataid = $dataPost['id'];
            unset($dataPost['id']);
    
            $result = $this->db->update("t_direksi_agenda" ,$dataPost, array('id_direksi_agenda' => $dataid));
    
    
            if ($result) {
                return true;
            }else{
                return false;
            }
            
            }elseif ($dataPost['proc'] == 'delete') {
            unset($dataPost['proc']);
            $dataid = $dataPost['id'];
            unset($dataPost['id']);
    
            $result = $this->db->delete("t_direksi_agenda", array('id_direksi_agenda' => $dataid));

            if ($result) {
                return true;
            }else{
                return false;
            }
            }else{
            return false;
            }
    }

    public function setProtokol($dataPost)
    {
        if ($dataPost['proc'] == 'insert') {

            unset($dataPost['proc']);
            unset($dataPost['id']);
            
            $result = $this->db->insert("t_protokol_agenda",$dataPost);
    
            $last_id = $this->db->insert_id();
    

            if ($result) {
                return true;
            }else{
                return false;
            }
    
            }elseif ($dataPost['proc'] == 'update') {
            unset($dataPost['proc']);
            $dataid = $dataPost['id'];
            unset($dataPost['id']);
            
            
    
            $result = $this->db->update("t_protokol_agenda" ,$dataPost, array('id_protokol_agenda' => $dataid));
    
    
            if ($result) {
                return true;
            }else{
                return false;
            }
            
            }elseif ($dataPost['proc'] == 'delete') {
            unset($dataPost['proc']);
            $dataid = $dataPost['id'];
            unset($dataPost['id']);
    
            $result = $this->db->delete("t_protokol_agenda", array('id_protokol_agenda' => $dataid));

            if ($result) {
                return true;
            }else{
                return false;
            }
            }else{
            return false;
            }
    }

    public function setKonfirmasi($postdata)
    {
        $id_direksi_agenda = $this->uri->segment(3);

        $update_array = array(
            'konfirmasi' => 1,
            'status' => $postdata['status'],
            'keterangan' => $postdata['keterangan'],
            'waktu_konfirmasi' => date("Y-m-d H:i:s")
         );

         $this->db->where("id_direksi_agenda", $id_direksi_agenda);

         $this->db->update("t_direksi_agenda", $update_array);

         redirect($postdata['page'].'?success=1');
         

    }



  private function uploadkan($iddata)
  { 

      $filename                       = "AGENDA_".uniqid();
      $config['upload_path']          = './UPLOADS/';
      $config['allowed_types']        = 'jpeg|jpg|gif|png|pdf|doc|docx|xls|xlxs';
      $config['max_size']             = 3000;
      $config['file_name']            = $filename;
    

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('foto'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $update_array = array(
            'foto' => $this->upload->data()['file_name'], 
          );

          $this->db->where("id_agenda", $iddata);
          $this->db->update("t_agenda", $update_array);
      }

  }



}