<?php

class M_direksi_agenda extends CI_Model{

    private function userblok_konfirmasi()
    {
        //YANG USER ITU PUNYA
        $LOGGED_AS = $this->data["LOGGED_AS"];
        

        if($LOGGED_AS == "DIREKSI" ){
            
            $USERDATA = $this->session->userdata();
            $this->db->where("t_direksi_agenda.id_direksi", $USERDATA["id_direksi"]);

        }

    }

    public function getAbsen()
    {

        $this->userblok_konfirmasi();
        //YANG TAK DIDELETE

        $this->db->join("t_direksi","t_direksi_agenda.id_direksi = t_direksi.id_direksi", "LEFT");
        $this->db->join("t_agenda","t_direksi_agenda.id_agenda = t_agenda.id_agenda", "LEFT");
        $this->db->where("t_direksi_agenda.konfirmasi","1");

        // echo "<pre>";
        // print_r($this->db->get('t_direksi_agenda')->result_array());
        // exit();
        
        return $this->db->get('t_direksi_agenda');
    }

    public function hadirkan()
    {
       $id_direksi_agenda = $this->uri->segment(3);

       $update_array = array(
           'hadir' => 1
        );

        $this->db->where("id_direksi_agenda", $id_direksi_agenda);

        $this->db->update("t_direksi_agenda", $update_array);

        redirect(base_url()."Absensi?success=1");

    }

    public function hadirkanProtokol()
    {
        $id_protokol_agenda = $this->uri->segment(3);

        $update_array = array(
            'absen' => 1
         );
 
         $this->db->where("id_protokol_agenda", $id_protokol_agenda);
 
         $this->db->update("t_protokol_agenda", $update_array);
 
         redirect(base_url()."AbsensiProtokol?success=1");
    }
    

    public function hadirkanSekretaris()
    {
        $id_sekdir_agenda = $this->uri->segment(3);

        $update_array = array(
            'absen' => 1
         );
 
         $this->db->where("id_sekdir_agenda", $id_sekdir_agenda);
 
         $this->db->update("t_sekdir_agenda", $update_array);
 
         redirect(base_url()."AbsensiSekretaris?success=1");
    }
    
  function set($dataPost){

        // if($dataPost["password"] == ""){
        // unset($dataPost["password"]);
        // }

        if ($dataPost['proc'] == 'insert') {

        unset($dataPost['proc']);
        unset($dataPost['id']);
        
        $result = $this->db->insert("t_direksi_agenda",$dataPost);

        $last_id = $this->db->insert_id();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($last_id);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }

        }elseif ($dataPost['proc'] == 'update') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);
        
        

        $result = $this->db->update("t_direksi_agenda" ,$dataPost, array('id_direksi_agenda' => $dataid));

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($dataid);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }
        
        }elseif ($dataPost['proc'] == 'delete') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);

        $result = $this->db->update("t_direksi_agenda", array('deleted' => 1), array('id_direksi_agenda' => $dataid));

        if ($result) {
            return true;
        }else{
            return false;
        }
        }else{
        return false;
        }


    }

    public function Get_absen_details($params)
    {
        if($params['status'] == 'DIREKSI'){

            $id_direksi_agenda = $params['id_direksi_agenda'];
            $this->db->select('nama_protokol as nama, kontak');
            $this->db->join("t_protokol","t_protokol_agenda.id_protokol = t_protokol.id_protokol", "LEFT");
            $this->db->join("t_direksi_agenda","t_direksi_agenda.id_agenda = t_protokol_agenda.id_agenda");
            $this->db->where("t_direksi_agenda.id_direksi_agenda",$id_direksi_agenda);
            $this->db->where("t_protokol_agenda.absen", 1);
            $data['protokol'] = $this->db->get('t_protokol_agenda')->result();
            
        }elseif($params['status'] == 'PROTOKOL'){
            $id_direksi_agenda = $params['id_protokol_agenda'];
            $this->db->select('nama_direksi as nama, kontak');
            $this->db->join("t_direksi","t_direksi_agenda.id_direksi = t_direksi.id_direksi", "LEFT");
            $this->db->where("t_direksi_agenda.id_direksi_agenda",$id_direksi_agenda);
            $this->db->where("t_direksi_agenda.hadir", 1);
            $data['direksi'] = $this->db->get('t_direksi_agenda')->result();
        }

        $this->db->select('nama_sekdir as nama, kontak');
        $this->db->join("t_sekdir","t_sekdir_agenda.id_sekdir = t_sekdir.id_sekdir", "LEFT");
        $this->db->join("t_direksi_agenda","t_direksi_agenda.id_agenda = t_sekdir_agenda.id_agenda");
        $this->db->where("t_direksi_agenda.id_direksi_agenda",$id_direksi_agenda);
        $this->db->where("t_sekdir_agenda.absen", 1);
        $data['sekretaris'] = $this->db->get('t_sekdir_agenda')->result();
        
        return $data;
    }



  private function uploadkan($iddata)
  { 

      $filename                       = "ADM_".uniqid();
      $config['upload_path']          = './UPLOADS/';
      $config['allowed_types']        = 'jpeg|jpg|gif|png';
      $config['max_size']             = 1000;
      $config['file_name']            = $filename;
    

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('foto'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $update_array = array(
            'foto' => $this->upload->data()['file_name'], 
          );

          $this->db->where("id_direksi_agenda", $iddata);
          $this->db->update("t_direksi_agenda", $update_array);
      }

  }



}