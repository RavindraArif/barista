<?php

class M_protokol extends CI_Model{
    public function get()
    {

        //YANG TAK DIDELETE
        $this->db->where("deleted","0");
        
        return $this->db->get('t_protokol');
    }

  function set($dataPost){

        if($dataPost["password"] == ""){
        unset($dataPost["password"]);
        }

        if ($dataPost['proc'] == 'insert') {

        unset($dataPost['proc']);
        unset($dataPost['id']);
        
        $result = $this->db->insert("t_protokol",$dataPost);

        $last_id = $this->db->insert_id();

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($last_id);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }

        }elseif ($dataPost['proc'] == 'update') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);
        
        

        $result = $this->db->update("t_protokol" ,$dataPost, array('id_protokol' => $dataid));

        if($_FILES['foto']['size'] > 0){
            $this->uploadkan($dataid);
        }

        if ($result) {
            return true;
        }else{
            return false;
        }
        
        }elseif ($dataPost['proc'] == 'delete') {
        unset($dataPost['proc']);
        $dataid = $dataPost['id'];
        unset($dataPost['id']);

        $result = $this->db->update("t_protokol", array('deleted' => 1), array('id_protokol' => $dataid));

        // $result = $this->db->delete("t_protokol", array('id_protokol' => $dataid));
        if ($result) {
            return true;
        }else{
            return false;
        }
        }else{
        return false;
        }


    }



  private function uploadkan($iddata)
  { 

      $filename                       = "PROTOKOL_".uniqid();
      $config['upload_path']          = './UPLOADS/';
      $config['allowed_types']        = 'jpeg|jpg|gif|png';
      $config['max_size']             = 1000;
      $config['file_name']            = $filename;
    

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload('foto'))
      {
              $error = array('error' => $this->upload->display_errors());
              print_r($error);
      }
      else
      {
          $update_array = array(
            'foto' => $this->upload->data()['file_name'], 
          );

          $this->db->where("id_protokol", $iddata);
          $this->db->update("t_protokol", $update_array);
      }

  }



}