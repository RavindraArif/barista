<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Harga extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{
		$this->data["dataset"] = $this->M_harga->get()->result_array();
		$this->data["kategori"] = $this->M_kategori->get()->result_array();

		$this->load->view('include/header', $this->data);
		$this->load->view('barista/harga', $this->data);
		$this->load->view('include/footer');
	}

	public function PostData()
	{
		$this->M_harga->set($this->input->post());
	}
}
