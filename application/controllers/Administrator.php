<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{

		$this->data['dataset'] = $this->M_administrator->get()->result_array();

		$this->load->view('include/header', $this->data);
		$this->load->view('barista/administrator', $this->data);
		$this->load->view('include/footer');
	}

	public function PostData()
	{
		$this->M_administrator->set($this->input->post());
	}

}
