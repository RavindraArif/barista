<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SekertarisDireksi extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{
		$this->data["dataset"] = $this->M_sekdir->get()->result_array();
		$this->load->view('include/header', $this->data);
		$this->load->view('barista/sekertarisdireksi', $this->data);
		$this->load->view('include/footer');
	}

	public function PostData()
	{
		$this->M_sekdir->set($this->input->post());
	}
}
