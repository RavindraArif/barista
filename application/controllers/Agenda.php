<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

    public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{   
        // echo "<pre>";
        // print_r($this->session->userdata());
        // exit();

        $this->data['dataset'] = $this->M_agenda->get()->result_array();

		$this->load->view('include/header', $this->data);
		$this->load->view('barista/agenda', $this->data);
		$this->load->view('include/footer');
    }
    
    public function LihatAgenda()
    {
        $tanggal_agenda = $this->uri->segment(3);

        $this->data['dataset'] = $this->M_agenda->getByDate($tanggal_agenda)->result_array();


		$this->load->view('include/header', $this->data);
		$this->load->view('barista/lihatagenda', $this->data);
		$this->load->view('include/footer');

    }

    public function DireksiAgenda()
    {
        $id_agenda = $this->uri->segment(3);
        $judul_agenda = urldecode($this->uri->segment(4));

        $this->data['dataset'] = $this->M_agenda->getDireksi($id_agenda)->result_array();
        $this->data['direksi'] = $this->M_direksi->get()->result_array();
        $this->data['judul_agenda'] = $judul_agenda;

        $this->load->view('include/header', $this->data);
		$this->load->view('barista/direksiagenda', $this->data);
		$this->load->view('include/footer');

    }

    public function ProtokolAgenda()
    {
        $id_agenda = $this->uri->segment(3);
        $judul_agenda = urldecode($this->uri->segment(4));

        $this->data['dataset'] = $this->M_agenda->getProtokol($id_agenda)->result_array();
        $this->data['protokol'] = $this->M_protokol->get()->result_array();
        $this->data['judul_agenda'] = $judul_agenda;

        $this->load->view('include/header', $this->data);
		$this->load->view('barista/protokolagenda', $this->data);
		$this->load->view('include/footer');
    }

    public function KonfirmasiAgenda()
    {
        $id_agenda = $this->uri->segment(3);
        $judul_agenda = urldecode($this->uri->segment(4));

        $this->data['dataset'] = $this->M_agenda->getDireksi($id_agenda)->result_array();
        $this->data['direksi'] = $this->M_direksi->get()->result_array();
        $this->data['judul_agenda'] = $judul_agenda;

        $this->load->view('include/header', $this->data);
		$this->load->view('barista/konfirmasiagenda', $this->data);
		$this->load->view('include/footer');
    }

	public function PostData()
	{
		$this->M_agenda->set($this->input->post());
    }
    
    public function PostDataDireksi()
    {
        $this->M_agenda->setDireksi($this->input->post());
    }

    public function PostDataProtokol()
    {
        $this->M_agenda->setProtokol($this->input->post());
    }

    public function PostDataKonfirmasi()
    {
        $this->M_agenda->setKonfirmasi($this->input->post());
    }

    public function PostDataGallery()
    {
        $this->M_gallery_agenda->set($this->input->post());
    }


    //HALAMAN DETAIL AGENDA

    public function DetailAgenda()
    {
        $id_agenda = $this->uri->segment(3);

        $this->data['dataset'] = $this->M_agenda->getByID($id_agenda)->result_array()[0];
        $this->data['sekretaris'] = $this->M_agenda->getSekretaris($id_agenda)->result_array();
        $this->data['konfirmasi'] = $this->M_agenda->getDireksi($id_agenda)->result_array();
        $this->data['protokol'] = $this->M_agenda->getProtokol($id_agenda)->result_array();
        $this->data["biaya"] = $this->M_biaya_agenda->get($id_agenda)->result_array();
        $this->data["gallery"] = $this->M_gallery_agenda->get($id_agenda)->result_array();
        
        
        $this->load->view('include/header', $this->data);
		$this->load->view('barista/detailagenda', $this->data);
		$this->load->view('include/footer');
    }
}
