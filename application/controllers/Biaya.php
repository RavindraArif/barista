<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biaya extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{

        $this->data["dataset"] = $this->M_agenda->getBiayaAgenda()->result_array();

			$this->load->view('include/header', $this->data);
			$this->load->view('barista/biaya_1', $this->data);
			$this->load->view('include/footer');
	}

	public function Kelola()
	{
		$id_agenda = $this->uri->segment(3);
		$judul = urldecode($this->uri->segment(4));

		$this->data["dataset"] = $this->M_biaya_agenda->get($id_agenda)->result_array();
		$this->data["harga"] = $this->M_harga->get()->result_array();

		$this->data["judul"] = $judul;

		$this->load->view('include/header', $this->data);
		$this->load->view('barista/biaya_2', $this->data);
		$this->load->view('include/footer');

	}

	public function PostData()
	{
		$this->M_biaya_agenda->set($this->input->post());
	}
    
}
