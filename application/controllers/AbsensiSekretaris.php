<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsensiSekretaris extends CI_Controller {

    public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{

        $this->data['dataset'] = $this->M_agenda->getSekretaris('0')->result_array();
        

		$this->load->view('include/header', $this->data);
		$this->load->view('barista/absensisekretaris', $this->data);
		$this->load->view('include/footer');
    }
    
    public function Hadirkan()
    {
        $this->M_direksi_agenda->hadirkanSekretaris();
    }

}
