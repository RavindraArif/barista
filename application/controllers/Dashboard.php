<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{
		redirect(base_url()."Agenda");
		// $this->load->view('include/header', $this->data);
		// $this->load->view('barista/home');
		// $this->load->view('include/footer');
	}
}
