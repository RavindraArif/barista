<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

    public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{

        $this->data['dataset'] = $this->M_direksi_agenda->getAbsen()->result_array();
        
		$this->load->view('include/header', $this->data);
		$this->load->view('barista/absensi', $this->data);
		$this->load->view('include/footer');
    }
    
    public function Hadirkan()
    {
        $this->M_direksi_agenda->hadirkan();
	}
	
	public function Get_absen_details()
	{
		$data = $this->M_direksi_agenda->Get_absen_details($this->input->post());	
		echo json_encode($data);
	}

}
