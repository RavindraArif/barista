<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StaffProtocol extends CI_Controller {

	public function __construct()
	{
	  parent::__construct();
	  cek_login();
	}

	public function index()
	{
		$this->data["dataset"] = $this->M_protokol->get()->result_array();
		$this->load->view('include/header', $this->data);
		$this->load->view('barista/staffprotocol', $this->data);
		$this->load->view('include/footer');
	}

	public function PostData()
	{
		$this->M_protokol->set($this->input->post());
	}
}
