<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login/login');
	}

	public function verifikasi()
	{

		if($this->login_admin() == true){
		}else{
			if($this->login_direksi() == true){
			}else{
				if($this->login_sekdir() == true){
				}else{
					if($this->login_protokol() == false){
						redirect(base_url()."Login");
					}
				}
			}	
		}

	}

	public function Logout()
	{
		$this->session->sess_destroy();
		redirect(base_url()."Login");
	}

	private function login_admin()
	{
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$this->db->where("username", $username);
		$this->db->where("password", $password);
		$data = $this->db->get("t_administrator")->result_array();

		if(count($data) > 0){
			$this->session->set_userdata($data[0]);
			$this->session->set_userdata(array('logged_as' => 'ADMINROOT'));
			
			redirect(base_url()."Dashboard");
		}

		return false;

	}


	private function login_direksi()
	{
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$this->db->where("username", $username);
		$this->db->where("password", $password);
		$data = $this->db->get("t_direksi")->result_array();

		if(count($data) > 0){
			$this->session->set_userdata($data[0]);
			$this->session->set_userdata(array('logged_as' => 'DIREKSI'));
			
			redirect(base_url()."Dashboard");
		}

		return false;

	}


	private function login_sekdir()
	{
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$this->db->where("username", $username);
		$this->db->where("password", $password);
		$data = $this->db->get("t_sekdir")->result_array();

		if(count($data) > 0){
			$this->session->set_userdata($data[0]);
			$this->session->set_userdata(array('logged_as' => 'SEKDIR'));
			
			redirect(base_url()."Dashboard");
		}

		return false;

	}

	private function login_protokol()
	{
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$this->db->where("username", $username);
		$this->db->where("password", $password);
		$data = $this->db->get("t_protokol")->result_array();

		if(count($data) > 0){
			$this->session->set_userdata($data[0]);
			$this->session->set_userdata(array('logged_as' => 'PROTOKOL'));
			
			redirect(base_url()."Dashboard");
		}

		return false;

	}
}
