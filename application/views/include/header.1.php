<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>BARISTA | Dashboard</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

  <script src="<?php echo base_url() ?>template/AdminLTE/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>template/AdminLTE/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?php echo base_url() ?>template/AdminLTE/plugins/sweetalert2/sweetalert2.min.js"></script>
  <style>
  
    .sidebar-dark-primary, .sidebar-dark-primary a{
      background-color:#d35400 !important;
    }

    .sidebar-dark-primary > a, .user-panel{
      border-bottom: 1px solid #e67e22 !important;
    }

  </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-orange navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>

      <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
      <li class="nav-item dropdown">
        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu navbar-orange">
          <li><a href="#" class="nav-link">Action</a></li>
          <li><a href="#" class="nav-link">Action</a></li>
          <li><a href="#" class="nav-link">Action</a></li>
          <li><a href="#" class="nav-link">Action</a></li>
          <li><a href="#" class="nav-link">Action</a></li>
        </ul>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
     
      <!-- Notifications Dropdown Menu -->

      
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url() ?>assets/img/gear.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b>BARISTA</b> | ADMIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url() ?>template/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $LOGGED_AS; ?></a>
        </div>
      </div>

      

      <!-- Sidebar Menu -->
      <nav class="mt-2">

      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->


          <li class="nav-item">
            <a href="<?php echo base_url() ?>Dashboard" class="nav-link">
              <i class="fas fa-fire nav-icon"></i>
              <p>Dashboard</p>
            </a>
          </li>

      </ul>


      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">Agenda</li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Agenda" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Data Agenda</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Absensi" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Absensi Direksi</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url() ?>AbsensiProtokol" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Absensi Protokol</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Biaya" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Biaya Agenda</p>
            </a>
          </li>
        
      </ul>

      <?php if($LOGGED_AS == "ADMINROOT" || $LOGGED_AS == "SEKDIR"){ ?>


      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">Standar Harga</li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Harga" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Data Harga</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>KategoriHarga" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Data Kategori</p>
            </a>
          </li>
        
        </ul>
        <?php } ?>

        <?php if($LOGGED_AS == "ADMINROOT"){ ?>


      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">Admin</li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Administrator" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Administrator</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Direksi" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Direksi</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>SekertarisDireksi" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Sekertaris Direksi</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>StaffProtocol" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Staff Protocol</p>
            </a>
          </li>
        </ul>

      <?php } ?>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

        <li class="nav-header">Profile</li>

          <li class="nav-item">
            <a href="<?php echo base_url() ?>Login/Logout" class="nav-link">
              <i class="fas fa-times nav-icon"></i>
              <p>Logout</p>
            </a>
          </li>

      </ul>



      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>