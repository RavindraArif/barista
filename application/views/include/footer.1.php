
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">KurniaTeknologi</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="<?php echo base_url() ?>template/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/dist/js/adminlte.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/dist/js/demo.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/chart.js/Chart.min.js"></script>
<!-- <script src="<?php echo base_url() ?>template/AdminLTE/dist/js/pages/dashboard2.js"></script> -->

<script>

const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2000
  });

  function Popup(text) {
    Toast.fire({
        type: 'success',
        title: text
    })

    setTimeout(
    function() 
    {
      window.location.reload();
    }, 2000);
  }

</script>

</body>
</html>
