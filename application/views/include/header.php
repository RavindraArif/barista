<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bs/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bs/dist/css/bootstrap-select.min.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  <script src="<?php echo base_url() ?>assets/bs/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>assets/bs/dist/js/bootstrap-select.min.js"></script>

  <style>

    .content-wrapper{
      padding-top:0px;
      padding-right:20px;
      padding-left:20px;
    }

    .navbar-default {
        background-color: #0491ac;
        border-color: #1f5675;
        border-radius: 0px;
    }

    .navbar-default .navbar-brand {
        color: #FFF;
    }
    .navbar-default .navbar-nav>li>a {
      color: #FFF;
      letter-spacing:2px;
    }
    .panel-default>.panel-heading {
        color: #FFF;
        background-color: #2c3e50;
        border-color: #ddd;
    }

  </style>
</head>
<body>


<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo base_url() ?>Agenda"><b style="letter-spacing:px">BARISTA</b></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->

  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url() ?>Agenda"><i class="fa fa-tachometer"></i> Dashboard</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> Agenda <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>Agenda">Data Agenda</a></li>
          <li><a href="<?php echo base_url() ?>Biaya">Biaya Agenda</a></li>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i> Laporan <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <?php if(
          $LOGGED_AS == "ADMINROOT" ||
            $LOGGED_AS == "DIREKSI" ||
            $LOGGED_AS == "SEKDIR" 
            ){ ?>
          <li><a href="<?php echo base_url() ?>Absensi">Absensi Direksi</a></li>
          <?php } ?>
          <?php if(
            $LOGGED_AS == "ADMINROOT" ||
            $LOGGED_AS == "PROTOKOL"
            ){ ?>

          <li><a href="<?php echo base_url() ?>AbsensiProtokol">Absensi Protokol</a></li>
            <?php } ?>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b><i class="fa fa-user"></i> <?php echo strtoupper($this->session->userdata('username')) ?></b> <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>Login/Logout">Logout</a></li>
        </ul>
      </li>
    </ul>

    <?php if(
      $LOGGED_AS == "ADMINROOT"
      ){ ?>

    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengaturan Admin <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>Administrator">Administrator</a></li>
          <li><a href="<?php echo base_url() ?>Direksi">Direksi</a></li>
          <li><a href="<?php echo base_url() ?>SekertarisDireksi">Sekretaris Direksi</a></li>
          <li><a href="<?php echo base_url() ?>StaffProtocol">Staff Protokol</a></li>
        </ul>
      </li>
    </ul>

    <?php } ?>

    <?php if(
      $LOGGED_AS == "ADMINROOT" ||
      $LOGGED_AS == "SEKDIR" 
      ){ ?>
   
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Harga & Kategori <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>Harga">Data Harga</a></li>
          <li><a href="<?php echo base_url() ?>KategoriHarga">Data Kategori</a></li>
        </ul>
      </li>
    </ul>

    <?php } ?>



  </div><!-- /.navbar-collapse -->
</nav>



