<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Biaya Agenda</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Biaya Agenda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Biaya Agenda</h3>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">
                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Kode Agenda</th>
                  <th>Judul Agenda</th>
                  <th>Waktu Agenda</th>
                  <th>Lokasi</th>
                  <th class="hilang">total_biaya</th>
                  <th>Total Biaya</th>
                  <?php if($LOGGED_AS == "PROTOKOL" || $LOGGED_AS == "ADMINROOT"){ ?>
                  <th></th>
                  <?php } ?>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_agenda'] ?>"  data-toggle="modal" data-target="#POPUPMODAL">
                    <td class="c-kode_agenda"><?php echo $value['kode_agenda'] ?></td>
                    <td class="c-judul_agenda"><?php echo $value['judul_agenda'] ?></td>
                    <td class="c-waktu_agenda"><?php echo $value['waktu_agenda'] ?></td>
                    <td class="c-lokasi_detail"><?php echo $value['lokasi_detail'] ?></td>
                    <td class="c-total_biaya hilang"><?php if($value['total_biaya'] == ""){ echo 0;}else{ echo $value['total_biaya'];} ?></td>
                    <td class="c-total_biaya"><?php if($value['total_biaya'] == ""){ echo rupiah(0);}else{ echo rupiah($value['total_biaya']);} ?></td>
                  <?php if($LOGGED_AS == "PROTOKOL" || $LOGGED_AS == "ADMINROOT"){ ?>
                    <td>
                        <a href="<?php echo base_url() ?>Biaya/Kelola/<?php echo $value["id_agenda"] ?>/<?php echo $value['judul_agenda'] ?>" class="btn btn-primary btn-sm btn-block">Kelola Pembiayaan</a>
                    </td>
                  <?php } ?>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();
    });

    //POST DATA
    var MODE = "";
    var ID = "";

    $("form[name='uploader']").submit(function(e) {

    var formData = new FormData($(this)[0]);

    formData.append("proc", MODE);
    formData.append("id", ID);

    $.ajax({
        url: "<?php echo base_url()?>Administrator/PostData",
        type: "POST",
        data: formData,
        success: function (msg) {
            window.location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
    });

    function simpan(){
    MODE = "insert";

    $(".form-control").each(function(){
        $(this).val("");
    });

    }

    $(".barisData").click(function(){
    MODE = "update";
    ID = $(this).attr("idData");
    var ROW = $(this);

    $(".form-control").each(function(){

        if($(this).attr("id")){

            //KECUALIKAN FILE
            if($(this).attr("id") != "txt-foto"){
                var col = $(this).attr("id");
                var idnya = "#" + col;
                var classnya = "." + col.replace("txt","c");

                $(idnya).val(ROW.find(classnya).text().trim());
            }
        
        }
    
    });

    });

    function hapus() {
    MODE = "delete";
    }


</script> 


<?php 
    
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

?>