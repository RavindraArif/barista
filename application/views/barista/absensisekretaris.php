<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Absensi Sekretaris</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Absensi Kehadiran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Absensi</h3>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">

                <?php if($this->input->get('success') == '1'){ ?>
                <div class="alert alert-success">
                  <strong>Sukses!</strong> berhasil melakukan absensi.
                </div>
                <?php } ?>

                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Judul Agenda</th>
                  <th>Lokasi Detail</th>
                  <th>Waktu Agenda</th>
                  <th>Nama Sekretaris</th>
                  <th>Kontak</th>
                  <th>Hadir</th>
                  <?php if($LOGGED_AS == "SEKDIR"){ ?>
                  <th></th>
                  <?php } ?>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_sekdir_agenda'] ?>">
                    <td class="c-judul_agenda"><?php echo $value['judul_agenda'] ?></td>
                    <td class="c-lokasi_detail"><?php echo $value['lokasi_detail'] ?></td>
                    <td class="c-waktu_agenda"><?php echo $value['waktu_agenda'] ?></td>
                    <td class="c-nama_sekdir"><?php echo $value['nama_sekdir'] ?></td>
                    <td class="c-kontak"><?php echo $value['kontak'] ?></td>
                    <td class=""><?php if( $value['absen'] == "1"){ echo "Hadir";}else{ echo "Belum Hadir"; } ?></td>
                  <?php if($LOGGED_AS == "SEKDIR"){ ?>
                    <td>
                        <a href="<?php echo base_url() ?>AbsensiSekretaris/Hadirkan/<?php echo $value['id_sekdir_agenda'] ?>" class="btn btn-primary btn-block btn-sm"> Konfirmasi Kehadiran </a>
                    </td>
                  <?php } ?>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>




<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();
    });


</script> 