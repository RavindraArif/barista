<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Agenda : <b><?php echo date('d F Y', strtotime($this->uri->segment(3))); ?></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Data Agenda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Agenda</h3>
                <?php if($LOGGED_AS == "ADMINROOT" || $LOGGED_AS == "SEKDIR"){?>
                <button type="button" class="btn btn-primary" style="float:right" data-toggle="modal" data-target="#POPUPMODAL" onclick="simpan()"><i class="fa fa-plus" style="margin-right:10px"></i>Tambah Agenda</button>
                <?php } ?>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">
                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Kode Agenda</th>
                  <th>Judul Agenda</th>
                  <th class="hilang">Waktu Agenda</th>
                  <th>Waktu Agenda</th>
                  <th>Lokasi</th>
                  <th>Direksi</th>
                  <th>Konfirmasi</th>
                  <th>Staff Protokol</th>
                  <th class="hilang">Gambar</th>
                  <th>Lampiran</th>
                  <!-- <th>Create Date</th> -->
                  <th>Last Update</th>

                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_agenda'] ?>">
                    <td class="c-kode_agenda"><?php echo $value['kode_agenda'] ?></td>
                    <td class="c-judul_agenda"><?php echo $value['judul_agenda'] ?></td>
                    <td class="c-waktu_agenda hilang"><?php echo $value['waktu_agenda'] ?></td>
                    <td class="c-jam"><?php echo explode(" ",$value['waktu_agenda'])[1] ?></td>
                    <td class="c-lokasi_detail"><?php echo $value['lokasi_detail'] ?></td>
                    <td>
                    <b><?php echo $value['jumlah_direksi'] ?></b> &nbsp;&nbsp;
                    <a href="<?php echo base_url() ?>Agenda/DireksiAgenda/<?php echo $value['id_agenda'] ?>/<?php echo $value['judul_agenda'] ?>" class="btn btn-sm btn-default"> <i class="fa fa-eye"></i> </a>
                    </td>
                    <td>
                    <b><?php echo $value['jumlah_konfirmasi'] ?>/<?php echo $value['jumlah_direksi'] ?></b> &nbsp;&nbsp;
                    <a href="<?php echo base_url() ?>Agenda/KonfirmasiAgenda/<?php echo $value['id_agenda'] ?>/<?php echo $value['judul_agenda'] ?>" class="btn btn-sm btn-default linkbtn"> <i class="fa fa-eye"></i> </a>
                    </td>
                    <td>
                    <b><?php echo $value['jumlah_protokol'] ?></b> &nbsp;&nbsp;
                    <a href="<?php echo base_url() ?>Agenda/ProtokolAgenda/<?php echo $value['id_agenda'] ?>" class="btn btn-sm btn-default"> <i class="fa fa-eye"></i> </a>
                    </td>
                    <td class="c-foto hilang"><?php echo $value['foto'] ?></td>
                    <td class="">

                        <?php if($value['foto'] != ""){ ?>
                          <a href="<?php echo base_url() ?>UPLOADS/<?php echo $value['foto']; ?>" class="btn btn-primary btn-sm btn-block"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Lihat Lampiran</a>
                        <?php } ?>
                    </td>
                    <!-- <td class=""><?php echo $value['create_date'] ?></td> -->

                    <td style="font-size:10px">
                    <button class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#POPUPMODAL"> <i class="edit"></i> Ubah Data</button>
                    <?php echo $value["last_update"] ?>
                    </td>

       
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


<?php if($LOGGED_AS == "ADMINROOT" || $LOGGED_AS == "SEKDIR"){?>
  <!-- form start -->
<form role="form" name="uploader" enctype="multipart/form-data">

<div id="POPUPMODAL" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width:100%; text-align:center">Form Agenda</h4>
      </div>
      <div class="modal-body">
                <div class="card-body">

                    <div class="form-group">
                        <label for="txt-kode_agenda">Kode Agenda</label>
                        <input type="text" class="form-control" id="txt-kode_agenda" name="kode_agenda">
                    </div>

                    <div class="form-group">
                        <label for="txt-judul_agenda">Judul Agenda</label>
                        <input type="text" class="form-control" id="txt-judul_agenda" name="judul_agenda">
                    </div>

                    <div class="form-group">
                        <label for="txt-jam">Waktu Agenda</label>
                        <input type="time" class="form-control" id="txt-jam">
                    </div>

                    <div class="form-group">
                        <label for="txt-lokasi_detail">Lokasi Detail</label>
                        <input type="text" class="form-control" id="txt-lokasi_detail" name="lokasi_detail">
                    </div>

                    <div class="form-group">
                        <label for="txt-foto">Gambar</label>
                        <input type="file" class="form-control" id="txt-foto" name="foto">
                    </div>

                </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="proc" value="Simpan" class="btn btn-success"><i class="fa fa-check" style="margin-right:10px"></i>Simpan</button>
        <button type="submit" name="proc" value = "Hapus" onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash" style="margin-right:10px"></i>Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

</form>

<?php } ?>

<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable(); 
    });

    //POST DATA
    var MODE = "";
    var ID = "";

    $("form[name='uploader']").submit(function(e) {

    var formData = new FormData($(this)[0]);

    formData.append("proc", MODE);
    formData.append("id", ID);
    formData.append("waktu_agenda", '<?php echo $this->uri->segment(3) ?> ' + $("#txt-jam").val());

    formData.append("last_update", "<?php echo $UPDATED_BY ?> On <?php echo date('d-m-Y H:i:s') ?>");

    $.ajax({
        url: "<?php echo base_url()?>Agenda/PostData",
        type: "POST",
        data: formData,
        success: function (msg) {

          Popup("Berhasil");
            // window.location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
    });

    function simpan(){
    MODE = "insert";

    $(".form-control").each(function(){
        $(this).val("");
    });

    }

    $(".barisData").click(function(){
    MODE = "update";
    ID = $(this).attr("idData");
    var ROW = $(this);

    $(".form-control").each(function(){

        if($(this).attr("id")){

            //KECUALIKAN FILE
            if($(this).attr("id") != "txt-foto"){
                var col = $(this).attr("id");
                var idnya = "#" + col;
                var classnya = "." + col.replace("txt","c");

                $(idnya).val(ROW.find(classnya).text().trim());
            }
        
        }
    
    });

    });

    function hapus() {
    MODE = "delete";
    }


</script> 