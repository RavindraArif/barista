<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Absensi Protokol</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Absensi Kehadiran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Absensi</h3>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">

                <?php if($this->input->get('success') == '1'){ ?>
                <div class="alert alert-success">
                  <strong>Sukses!</strong> berhasil melakukan absensi.
                </div>
                <?php } ?>

                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Judul Agenda</th>
                  <th>Lokasi Detail</th>
                  <th>Waktu Agenda</th>
                  <th>Nama Protokol</th>
                  <th>Kontak</th>
                  <th>Hadir</th>
                  <?php if($LOGGED_AS == "PROTOKOL"){ ?>
                  <th></th>
                  <?php } ?>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>">
                    <td class="c-judul_agenda"><?php echo $value['judul_agenda'] ?></td>
                    <td class="c-lokasi_detail"><?php echo $value['lokasi_detail'] ?></td>
                    <td class="c-waktu_agenda"><?php echo $value['waktu_agenda'] ?></td>
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                    <td class="c-kontak"><?php echo $value['kontak'] ?></td>
                    <td class=""><?php if( $value['absen'] == "1"){ echo "Hadir";}else{ echo "Belum Hadir"; } ?></td>
                  <?php if($LOGGED_AS == "PROTOKOL"){ ?>
                    <td>
                        <a href="<?php echo base_url() ?>AbsensiProtokol/Hadirkan/<?php echo $value['id_protokol_agenda'] ?>" class="btn btn-primary btn-block btn-sm"> Konfirmasi Kehadiran </a>
                    </td>
                  <?php } ?>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->

          <div class="col-md-4 box">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Absensi Details</h3>
              </div>
              <div class="card-body">
                <!-- Date dd/mm/yyyy -->
                <div class="form-group direksi">
                  <label>Direksi </label>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <input disabled type="text" class="form-control" data-mask>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                      <input disabled type="text" class="form-control" data-mask>
                    </div>
                  </div>
                </div>
                <!-- /.form group -->
                <div class="form-group sekretaris">
                  <label>Sekretaris </label>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <input disabled type="text" class="form-control" data-mask>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                      <input disabled type="text" class="form-control" data-mask>
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- /.card-body -->
            </div>
          </div>
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>




<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();
    });

    $(".barisData").click(function(){
        ID = $(this).attr("idData");
        $.ajax({
            url: "<?php echo base_url()?>Absensi/Get_absen_details",
            type: "POST",
            data: {id_protokol_agenda:ID,status:'PROTOKOL'},
            dataType: 'json',
            success: function (response) {
              var array_response = [response.direksi, response.sekretaris];
              var array_object = ['direksi', 'sekretaris'];
              var array_key = ['nama','kontak'];
              var array_icon = ['fa fa-user','fa fa-phone'];
              for(i=0;i<array_response.length;i++){
                console.log(array_response[i].length);
                if(array_response[i].length > 0 ){
                  var element = document.querySelector('.'+array_object[i]);
                  var label = document.createElement("label");
                  element.innerHTML = '';
                  label.innerText = array_object[i].charAt(0).toUpperCase() + array_object[i].slice(1);
                  element.appendChild(label);
                  for(j=0;j<array_response[i].length;j++){
                    for(k=0;k<array_key.length;k++){
                      console.log(array_response[i][j][array_key[k]]);
                      var input = document.createElement("input");
                      var div = document.createElement("div");
                      var innerDiv = document.createElement("div");
                      var span = document.createElement("span");
                      var icon = document.createElement("i");  
                      div.className = "form-group";
                      element.appendChild(div);
                      
                      innerDiv.className = "input-group";
                      div.appendChild(innerDiv);
                      
                      span.className = "input-group-addon";
                      innerDiv.appendChild(span);

                      icon.className = array_icon[k];
                      span.appendChild(icon);

                      input.type = "text";
                      input.className = "form-control";
                      input.disabled = true;
                      input.value = array_response[i][j][array_key[k]];
                      innerDiv.appendChild(input);
                    }
                  }
                }else{
                  var element = document.querySelector('.'+array_object[i]);
                  var label = document.createElement("label");
                  element.innerHTML = '';
                  label.innerText = array_object[i].charAt(0).toUpperCase() + array_object[i].slice(1);
                  element.appendChild(label);
                  for(k=0;k<array_key.length;k++){
                      var input = document.createElement("input");
                      var div = document.createElement("div");
                      var innerDiv = document.createElement("div");
                      var span = document.createElement("span");
                      var icon = document.createElement("i");  
                      div.className = "form-group";
                      element.appendChild(div);
                      
                      innerDiv.className = "input-group";
                      div.appendChild(innerDiv);
                      
                      span.className = "input-group-addon";
                      innerDiv.appendChild(span);

                      icon.className = array_icon[k];
                      span.appendChild(icon);

                      input.type = "text";
                      input.className = "form-control";
                      input.disabled = true;
                      innerDiv.appendChild(input);
                    }
                }
              }
              //Popup("Berhasil");
            },
        });
    });

</script> 