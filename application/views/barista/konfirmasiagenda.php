<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Konfirmasi Agenda : <b><?php echo $judul_agenda ?></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Konfirmasi Agenda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Konfirmasi Agenda</h3>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">

                <?php if($this->input->get('success') == '1'){ ?>
                <div class="alert alert-success">
                  <strong>Sukses!</strong> Anda telah mengkonfirmasi kehadiran.
                </div>
                <?php } ?>

                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th class="hilang">id_direksi</th>
                  <th>Nama Direksi</th>
                  <th class="hilang">konfirmasi</th>
                  <th>Konfirmasi</th>
                  <th>Status</th>
                  <th>Keterangan</th>
                  <th>Waktu Konfirmasi</th>
                  <?php if($LOGGED_AS == "DIREKSI"){ ?>
                  <th></th>
                  <?php } ?>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_direksi_agenda'] ?>">
                    <td class="c-id_direksi hilang"><?php echo $value['id_direksi'] ?></td>
                    <td class="c-nama_direksi"><?php echo $value['nama_direksi'] ?></td>
                    <td class="c-konfirmasi hilang"><?php echo $value['konfirmasi'] ?></td>
                    <td class=""><?php if($value['konfirmasi'] == 1){ echo "Sudah";}else{ echo "Belum"; } ?></td>
                    <td class="c-status"><?php echo $value['status'] ?></td>
                    <td class="c-keterangan"><?php echo $value['keterangan'] ?></td>
                    <td class="c-waktu_konfirmasi"><?php echo $value['waktu_konfirmasi'] ?></td>

                    <?php if($LOGGED_AS == "DIREKSI"){ 
                        if($value["id_direksi"] == $this->session->userdata("id_direksi")){
                      ?>
                    <td>

                      <form action="<?php echo base_url() ?>Agenda/PostDataKonfirmasi/<?php echo $value['id_direksi_agenda'] ?>" method="post">
                      
                        <input type="text" value="<?php echo current_url(); ?>" name="page" style="display:none">

                        <div class="form-group">
                          <label>Status:</label>
                          <select class="form-control" name="status">
                            <option>KONFIRMASI</option>
                            <option>TERUSKAN</option>
                          </select>
                        </div>

                        <div class="form-group">
                          <label>Keterangan:</label>
                          <input type="text" class="form-control" name="keterangan">
                        </div>

                        <input type="submit" value="Konfirmasi" class="btn btn-success btn-sm btn-block">

                      </form>

                    </td>
                    <?php }else{?>
                      <td></td>
                <?php }} ?>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();

    });

</script> 