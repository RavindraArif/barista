<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Protokol Agenda : <b><?php echo $judul_agenda ?></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Protokol Agenda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Protokol Terdaftar di Agenda</h3>
                <?php if($LOGGED_AS == "ADMINROOT" || $LOGGED_AS == "SEKDIR"){?>

                <button type="button" class="btn btn-primary" style="float:right" data-toggle="modal" data-target="#POPUPMODAL" onclick="simpan()"><i class="fa fa-plus" style="margin-right:10px"></i>Tambah Protokol ke Agenda</button>
                <?php } ?>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">
                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th class="hilang">id_direksi</th>
                  <th>Nama Protokol</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>"  data-toggle="modal" data-target="#POPUPMODAL">
                    <td class="c-id_direksi hilang"><?php echo $value['id_direksi'] ?></td>
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php if($LOGGED_AS == "ADMINROOT" || $LOGGED_AS == "SEKDIR"){?>

  <!-- form start -->
<form role="form" name="uploader" enctype="multipart/form-data">

<div id="POPUPMODAL" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width:100%; text-align:center">Form Tambah Protokol Agenda</h4>
      </div>
      <div class="modal-body">
                <div class="card-body">

                    <div class="form-group">
                    <label for="txt-id_protokol">Protokol:</label>
                    <select class="form-control" id="txt-id_protokol" name="id_protokol">
                        <?php foreach ($protokol as $key => $value) {?>
                        <option value='<?php echo $value["id_protokol"] ?>'><?php echo $value['nama_protokol'] ?></option>
                        <?php } ?>
                    </select>
                    </div>

                </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="proc" value="Simpan" class="btn btn-success"><i class="fa fa-check" style="margin-right:10px"></i>Simpan</button>
        <button type="submit" name="proc" value = "Hapus" onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash" style="margin-right:10px"></i>Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

</form>

      <?php } ?>


<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();
    });

    //POST DATA
    var MODE = "";
    var ID = "";

    $("form[name='uploader']").submit(function(e) {

    var formData = new FormData($(this)[0]);

    formData.append("proc", MODE);
    formData.append("id", ID);
    formData.append("id_agenda", '<?php echo $this->uri->segment(3) ?>');

    $.ajax({
        url: "<?php echo base_url()?>Agenda/PostDataProtokol",
        type: "POST",
        data: formData,
        success: function (msg) {
          Popup("Berhasil");
            // window.location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
    });

    function simpan(){
    MODE = "insert";

    $(".form-control").each(function(){
        $(this).val("");
    });

    }

    $(".barisData").click(function(){
    MODE = "update";
    ID = $(this).attr("idData");
    var ROW = $(this);

    $(".form-control").each(function(){

        if($(this).attr("id")){

            //KECUALIKAN FILE
            if($(this).attr("id") != "txt-foto"){
                var col = $(this).attr("id");
                var idnya = "#" + col;
                var classnya = "." + col.replace("txt","c");

                $(idnya).val(ROW.find(classnya).text().trim());
            }
        
        }
    
    });

    });

    function hapus() {
    MODE = "delete";
    }


</script> 