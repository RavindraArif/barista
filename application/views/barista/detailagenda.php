<style>
  .hilang{ display:none; }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Agenda : <b><?php echo strtoupper($dataset['judul_agenda']); ?></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Agenda</a></li>
              <li class="breadcrumb-item active">Detail Agenda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content" style="padding:20px">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><b>SUMMARY AGENDA</b>
          <hr></h3>

        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              <div class="row">
                <div class="col-12 col-sm-3">

                  <div class="panel panel-default">
                    <div class="panel-heading">Direksi</div>
                    <div class="panel-body"><?php echo $dataset['jumlah_direksi']; ?> orang</div>
                  </div>
                 
                </div>
                <div class="col-12 col-sm-3">

                <div class="panel panel-default">
                    <div class="panel-heading">Konfirmasi</div>
                    <div class="panel-body"><?php echo $dataset['jumlah_konfirmasi']; ?>/<?php echo $dataset['jumlah_direksi']; ?></div>
                  </div>

                </div>
                <div class="col-12 col-sm-3">

                  <div class="panel panel-default">
                    <div class="panel-heading">Staff Protokol</div>
                    <div class="panel-body"><?php echo $dataset['jumlah_protokol']; ?> orang</div>
                  </div>


                </div>
                <div class="col-12 col-sm-3">

                <div class="panel panel-default">
                    <div class="panel-heading">Pembiayaan</div>
                    <div class="panel-boc-hdy"><?php echo rupiah($dataset["total_biaya"]); ?></div>
                  </div>

                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-12">
                  <h4 style="width:100%"><b>Direksi dan Konfirmasi</b>
                  <a href="<?php echo base_url() ?>Agenda/KonfirmasiAgenda/<?php echo $this->uri->segment(3) ?>/<?php echo $dataset['judul_agenda'] ?>" class="btn btn-primary" style="position:relative; float:right;"><i class="fa fa-edit"></i> Kelola Konfirmasi</a>
                  <a href="<?php echo base_url() ?>Agenda/DireksiAgenda/<?php echo $this->uri->segment(3) ?>/<?php echo $dataset['judul_agenda'] ?>" class="btn btn-success" style="position:relative; float:right; margin-right:10px;"><i class="fa fa-edit"></i> Kelola Direksi</a>
                  </h4>
                  <br>
                    <div class="post">
                    
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>

                        <tr>
                        <th>Nama Direksi</th>
                        <th>Konfirmasi</th>
                        <th>Status</th>
                        <th>Keterangan</th>
                        <th>Waktu Konfirmasi</th>
                        </tr>

                        </thead>
                        <tbody>



                        <?php foreach ($konfirmasi as $key => $value) { ?>

                        <tr class="barisData" idData="<?php echo $value['id_direksi_agenda'] ?>">
                            <td class="c-nama_direksi"><?php echo $value['nama_direksi'] ?></td>
                            <td class=""><?php if($value['konfirmasi'] == 1){ echo "Sudah";}else{ echo "Belum"; } ?></td>
                            <td class="c-status"><?php echo $value['status'] ?></td>
                            <td class="c-keterangan"><?php echo $value['keterangan'] ?></td>
                            <td class="c-waktu_konfirmasi"><?php echo $value['waktu_konfirmasi'] ?></td>
                           
                        </tr>

                        <?php } ?>
                        
                        </tbody>
                        
                    </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->

                  </div>


                  <div class="col-12">
                  <h4 style="width:100%"><b>Absen Sekretaris</b>
                  <a href="<?php echo base_url() ?>AbsensiSekretaris" class="btn btn-primary" style="position:relative; float:right;"><i class="fa fa-edit"></i> Kelola Absensi</a>
                  </h4>
                  <br>
                    <div class="post">
                        
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>

                        <tr>
                        <th>Nama Sekretaris</th>
                        <th>Kontak</th>
                        <th>Absensi</th>
                        </tr>

                        </thead>
                        <tbody>



                        <?php foreach ($sekretaris as $key => $value) { ?>

                        <tr class="barisData" idData="<?php echo $value['id_sekdir_agenda'] ?>">
                            <td class="c-nama_sekdir"><?php echo $value['nama_sekdir'] ?></td>
                            <td class="c-kontak"><?php echo $value['kontak'] ?></td>
                            <td class="c-hadir"><?php if($value['absen'] == '1'){ echo 'Hadir';}else{ echo 'Belum Hadir';} ?></td>
                           
                        </tr>

                        <?php } ?>
                        
                        </tbody>
                        

                        </tfoot>
                    </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->

                  </div>
                  
                  <h4 style="width:100%"><b>Absen Protokol</b>
                    <a href="<?php echo base_url() ?>AbsensiProtokol" class="btn btn-primary" style="position:relative; float:right;"><i class="fa fa-edit"></i> Kelola Absensi</a></h4>
                    <br>
                    <div class="post clearfix">
                      
                    <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Nama Protokol</th>
                  <th>Absen</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($protokol as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>">
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                    <td class="c-absen"><?php if($value['absen'] == '1'){ echo 'Hadir';}else{ echo 'Belum Hadir';} ?></td>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>
                  

                    <h4 style="width:100%"><b>Staff Protokol</b>
                  <a href="<?php echo base_url() ?>Agenda/ProtokolAgenda/<?php echo $this->uri->segment(3) ?>/<?php echo $dataset['judul_agenda'] ?>" class="btn btn-success" style="position:relative; float:right; margin-right:10px;"><i class="fa fa-edit"></i> Kelola Protokol</a>
                  </h4>
                  <br>
                    <div class="post clearfix">
                      
                    <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Nama Protokol</th>
                  <th>Kontak</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($protokol as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>">
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                    <td class="c-kontak_protokol"><?php echo $value['kontak'] ?></td>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>


                  <h4 style="width:100%"><b>Absen Protokol</b>
                    <a href="<?php echo base_url() ?>AbsensiProtokol" class="btn btn-primary" style="position:relative; float:right;"><i class="fa fa-edit"></i> Kelola Absensi</a></h4>
                    <br>
                    <div class="post clearfix">
                      
                    <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Nama Protokol</th>
                  <th>Absen</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($protokol as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>">
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                    <td class="c-absen"><?php if($value['absen'] == '1'){ echo 'Hadir';}else{ echo 'Belum Hadir';} ?></td>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>

                    <h4 style="width:100%"><b>Absen Protokol</b>
                    <a href="<?php echo base_url() ?>AbsensiProtokol" class="btn btn-primary" style="position:relative; float:right;"><i class="fa fa-edit"></i> Kelola Absensi</a></h4>
                    <br>
                    <div class="post clearfix">
                      
                    <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Nama Protokol</th>
                  <th>Absen</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($protokol as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_protokol_agenda'] ?>">
                    <td class="c-nama_protokol"><?php echo $value['nama_protokol'] ?></td>
                    <td class="c-absen"><?php if($value['absen'] == '1'){ echo 'Hadir';}else{ echo 'Belum Hadir';} ?></td>
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>



                    <h4 style="width:100%"><b>Detail Pembiayaan</b>
                  <a href="<?php echo base_url() ?>Biaya/Kelola/<?php echo $this->uri->segment(3) ?>/<?php echo $dataset['judul_agenda'] ?>" class="btn btn-success" style="position:relative; float:right; margin-right:10px;"><i class="fa fa-edit"></i> Kelola Pembiayaan</a>
                    </h4>
                  <br>
                    <div class="post clearfix">
                      
                    <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Nama Barang / Jasa</th>
                  <th>Harga</th>
                  <th>Qty</th>
                  <th>Subtotal</th>
                  <th>Bukti Foto</th>
                  <th>Create Date</th>
                </tr>

                </thead>
                <tbody>



                <?php foreach ($biaya as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_biaya_agenda'] ?>">
                    <td class="c-nama_barang_jasa"><?php echo $value['nama_barang_jasa'] ?></td>
                    <td class=""><?php echo rupiah($value['harga']) ?></td>
                    <td class="c-qty"><?php echo $value['qty'] ?></td>
                    <td class=""><?php echo rupiah($value['qty'] * $value['harga']) ?></td>
                    <td class=""><img src="<?php echo base_url()."UPLOADS/".$value['foto'] ?>" alt="" style="width:50px;height:50px"></td>
                    <td class="c-create_date"><?php echo $value['create_date'] ?></td>
                
                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>


                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>

                    <h4><b>Gallery Agenda</b> 
                    <button type="button" class="btn btn-primary btn-sm" style="float:right" data-toggle="modal" data-target="#POPUPMODAL" onclick="simpan()"><i class="fa fa-plus" style="margin-right:10px"></i>Tambah Foto</button>    
                  </h4>
                    <div class="post clearfix">
                      
                    
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>

                        <tr>
                          <th class="hilang">Foto</th>
                          <th>Foto</th>
                          <th>Keterangan</th>
                        </tr>

                        </thead>
                        <tbody>



                        <?php foreach ($gallery as $key => $value) { ?>

                        <tr class="barisData" idData="<?php echo $value['id_gallery_agenda'] ?>"  data-toggle="modal" data-target="#POPUPMODAL">
                            <td class="c-foto hilang"><?php echo $value['foto'] ?></td>
                            <td class=""><img src="<?php echo base_url()."UPLOADS/".$value['foto'] ?>" alt="" style="width:100%"></td>
                            <td class="c-keterangan"><?php echo $value['keterangan'] ?></td>

                          </tr>

                        <?php } ?>
                        
                        </tbody>
                        
                      </table>
                      

                      <!-- <p>
                        <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                      </p> -->
                    </div>

                </div>
              </div>
            </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
              
              <div class="text-muted">
                <p class="text-sm">Kode Agenda
                  <b class="d-block"><?php echo $dataset['kode_agenda']; ?></b>
                </p>

                <p class="text-sm">Waktu Agenda
                  <b class="d-block"><?php echo $dataset['waktu_agenda']; ?></b>
                </p>
                <p class="text-sm">Lokasi Agenda
                  <b class="d-block"><?php echo $dataset['lokasi_detail']; ?></b>
                </p>
              </div>

              
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>



  <!-- form start -->
<form role="form" name="uploader" enctype="multipart/form-data">

<div id="POPUPMODAL" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width:100%; text-align:center">Upload Foto</h4>
      </div>
      <div class="modal-body">
                <div class="card-body">

                   


                    <div class="form-group">
                        <label for="txt-foto">Foto</label>
                        <input type="file" class="form-control" id="txt-foto" name="foto" accept="image/*">
                    </div>

                    <div class="form-group">
                        <label for="txt-keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="txt-keterangan" name="keterangan">
                    </div>

                </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="proc" value="Simpan" class="btn btn-success"><i class="fa fa-check" style="margin-right:10px"></i>Simpan</button>
        <button type="submit" name="proc" value = "Hapus" onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash" style="margin-right:10px"></i>Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

</form>



<script>

//DATA TABLES
// $(function () {
//     $('#example1').DataTable();
// });

//POST DATA
var MODE = "";
var ID = "";

$("form[name='uploader']").submit(function(e) {

var formData = new FormData($(this)[0]);

formData.append("proc", MODE);
formData.append("id", ID);
formData.append("id_agenda", "<?php echo $this->uri->segment(3) ?>");

$.ajax({
    url: "<?php echo base_url()?>Agenda/PostDataGallery",
    type: "POST",
    data: formData,
    success: function (msg) {
        window.location.reload();
    },
    cache: false,
    contentType: false,
    processData: false
});

e.preventDefault();
});

function simpan(){
MODE = "insert";

$(".form-control").each(function(){
    $(this).val("");
});

}

$(".barisData").click(function(){
MODE = "update";
ID = $(this).attr("idData");
var ROW = $(this);

$(".form-control").each(function(){

    if($(this).attr("id")){

        //KECUALIKAN FILE
        if($(this).attr("id") != "txt-foto"){
            var col = $(this).attr("id");
            var idnya = "#" + col;
            var classnya = "." + col.replace("txt","c");

            $(idnya).val(ROW.find(classnya).text().trim());
        }
    
    }

});

});

function hapus() {
MODE = "delete";
}


</script> 





  <?php 
    
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

?>