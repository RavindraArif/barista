
<?php 

// print_r(
//   json_encode($dataset)
// );
// exit();
?>
<!-- fullCalendar -->
<link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar/main.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-daygrid/main.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-timegrid/main.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-bootstrap/main.min.css">


<!-- fullCalendar 2.2.5 -->
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar/main.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-daygrid/main.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-timegrid/main.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-interaction/main.min.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/fullcalendar-bootstrap/main.min.js"></script>


<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }

    table{
      cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">

    <div class="panel panel-default">
      <div class="panel-heading"><b>KALENDER AGENDA</b></div>
      <div class="panel-body">

          <!-- <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1></h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Agenda</a></li>
                    <li class="breadcrumb-item active">Kalender Agenda</li>
                  </ol>
                </div>
              </div>
            </div>
          </section> -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
             
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">
                
                    <!-- CALENDAR -->
                    <div id="calendar"></div>
                    <!-- CALENDAR -->

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->


      </div>
    </div>


  </div>


  <script>

$( document ).ready(function() {

    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    var dataset = jQuery.parseJSON('<?php echo json_encode($dataset); ?>');

    var events = [];

    for (const key in dataset) {

      var datex = dataset[key].waktu_agenda;

      var date2 = new Date(datex);
      var d2    = date2.getDate(),
        m2    = date2.getMonth(),
        y2    = date2.getFullYear()

      events.push({
          id_agenda      : dataset[key].id_agenda,
          title          : dataset[key].kode_agenda + " \n" + dataset[key].judul_agenda,
          start          : new Date(y2, m2, d2),
          textColor      : '#FFF',
          backgroundColor: '#2980b9', //red
          borderColor    : '#2980b9', //red
          allDay         : true
      });
    }


    // console.log(dataset);
    // console.log(events);


    var calendar = new Calendar(calendarEl, {
      plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      //Random default events
      events    : events,
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function(info) {
        // is the "remove after drop" checkbox checked?
        if (checkbox.checked) {
          // if so, remove the element from the "Draggable Events" list
          info.draggedEl.parentNode.removeChild(info.draggedEl);
        }
      },
      dateClick: function(info) {
            window.location.href = "<?php echo base_url() ?>Agenda/LihatAgenda/" + info.dateStr;
            // alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
      },
      eventClick: function(info) {
          var id_agenda = info.event.extendedProps.id_agenda;
          window.location.href = "<?php echo base_url() ?>Agenda/DetailAgenda/" + id_agenda;
      }
    });

    calendar.render();

});


</script>