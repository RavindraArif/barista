<style>
    .hilang{
        display : none;
    }
    .barisData{
        cursor : pointer;
    }
</style>


<div class="content-wrapper" style="min-height: 1228.23px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengaturan Harga</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar Harga</a></li>
              <li class="breadcrumb-item active">Harga</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-default" >
              <div class="card-header">
                <h3 class="card-title">Tabel Harga</h3>
                <button type="button" class="btn btn-primary" style="float:right" data-toggle="modal" data-target="#POPUPMODAL" onclick="simpan()"><i class="fa fa-plus" style="margin-right:10px"></i>Tambah Harga</button>
              </div>
              <!-- /.card-header -->

              <div class="row">
                <div class="col-md-12" style="padding:20px">
                <table id="example1" class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th class="hilang">id_kategori</th>
                  <th>Kategori</th>
                  <th>Barang / Jasa</th>
                  <th class="hilang">harga</th>
                  <th>Harga</th>
                  <th>Last Update</th>

                </tr>

                </thead>
                <tbody>

                <?php foreach ($dataset as $key => $value) { ?>

                <tr class="barisData" idData="<?php echo $value['id_harga'] ?>"  data-toggle="modal" data-target="#POPUPMODAL">
                    <td class="c-id_kategori hilang"><?php echo $value['id_kategori'] ?></td>
                    <td class=""><?php echo $value['nama_kategori'] ?></td>
                    <td class="c-nama_barang_jasa"><?php echo $value['nama_barang_jasa'] ?></td>
                    <td class="c-harga hilang"><?php echo $value['harga'] ?></td>
                    <td class=""><?php echo rupiah($value['harga']) ?></td>
                    <td style="font-size:10px"><?php echo $value["last_update"] ?></td>

                </tr>

                <?php } ?>
                
                </tbody>
                
              </table>

                </div>

              </div>
            
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


  <!-- form start -->
<form role="form" name="uploader" enctype="multipart/form-data">

<div id="POPUPMODAL" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width:100%; text-align:center">Form Harga</h4>
      </div>
      <div class="modal-body">
                <div class="card-body">

                <div class="form-group">
                <label for="txt-id_kategori">Kategori:</label>
                <select class="form-control" id="txt-id_kategori" name="id_kategori">
                    <?php foreach ($kategori as $key => $value) {?>
                    <option value='<?php echo $value["id_kategori"] ?>'><?php echo $value['nama_kategori'] ?></option>
                    <?php } ?>
                </select>
                </div>

                <div class="form-group">
                    <label for="txt-nama_barang_jasa">Nama Barang / Jasa</label>
                    <input type="text" class="form-control" id="txt-nama_barang_jasa" name="nama_barang_jasa">
                </div>

                <div class="form-group">
                    <label for="txt-harga">Harga</label>
                    <input type="number" class="form-control" id="txt-harga" name="harga">
                </div>

                </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="proc" value="Simpan" class="btn btn-success"><i class="fa fa-check" style="margin-right:10px"></i>Simpan</button>
        <button type="submit" name="proc" value = "Hapus" onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash" style="margin-right:10px"></i>Hapus</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

</form>


<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>template/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

  <script>

    //DATA TABLES
    $(function () {
        $('#example1').DataTable();
    });

    //POST DATA
    var MODE = "";
    var ID = "";

    $("form[name='uploader']").submit(function(e) {

    var formData = new FormData($(this)[0]);

    formData.append("proc", MODE);
    formData.append("id", ID);

    formData.append("last_update", "<?php echo $UPDATED_BY ?> \nOn <?php echo date('d-m-Y H:i:s') ?>");

    $.ajax({
        url: "<?php echo base_url()?>Harga/PostData",
        type: "POST",
        data: formData,
        success: function (msg) {

          Popup("Berhasil");
            // window.location.reload();
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
    });

    function simpan(){
    MODE = "insert";

    $(".form-control").each(function(){
        $(this).val("");
    });

    }

    $(".barisData").click(function(){
    MODE = "update";
    ID = $(this).attr("idData");
    var ROW = $(this);
    $(".form-control").each(function(){

        if($(this).attr("id")){

            //KECUALIKAN FILE
            if($(this).attr("id") != "txt-foto"){
                var col = $(this).attr("id");
                var idnya = "#" + col;
                var classnya = "." + col.replace("txt","c");

                $(idnya).val(ROW.find(classnya).text().trim());
            }
        
        }
    
    });

    });

    function hapus() {
    MODE = "delete";
    }


</script> 


<?php 
    
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}

?>